= CHANGELOG for postgis

This file is used to list changes made in each version of postgis.

== 0.1.0:

* Initial release of postgis
